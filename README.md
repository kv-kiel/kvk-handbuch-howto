# KVK-Handbuch Howto

Alles, was man wissen muss, um am 
[KVK-Handbuch ](http://kv-kiel.gitlab.io/kvk-handbuch)
mitwirken zu können!

Das Ergebnis dieses Repositories wird unter
[GitLab-Pages](https://kv-kiel.gitlab.io/kvk-handbuch-howto)
veröffentlicht.
