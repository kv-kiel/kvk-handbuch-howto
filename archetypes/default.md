---
title                   : "{{ replace .Name "-" " " | title }}"
tags                    : []
date                    : {{ .Date }}
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---
