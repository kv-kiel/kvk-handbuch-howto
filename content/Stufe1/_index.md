---
title	: Simplizissimus
weight	: 10
pre		: "<b>1. </b>"
chapter : true
date    : 2020-01-07T20:27:02+01:00
LastModifierDisplayName :  "BalticPaddler"
LastModifierEmail : "howto@kv-kiel.de"
---

### Stufe 1

# Simplizissimus

Wenn man mit nichts etwas zu tun haben will, sondern nur Informationen liefern,
dann ist das vollkommen okay.

Es kann sein, dass du einfach nur findest, im
[KVK-Handbuch](https://kv-kiel.gitlab.io/kvk-handbuch/)
fehlt ein bestimmtes Thema. Oder du hast eine Frage, auf die du dort keine
Antwort gefunden hast.

Dann schick' uns einfach deine Frage oder das fehlende Thema und möglichst viele
Informationen, die uns helfen können. Auch Bilder sind eine gute Idee, denn die
lockern die Form sehr auf und --- ein Bild sagt mehr als tausend Worte!

Das Format, in dem du deinen Beitrag einreichst, ist relativ belanglos.
Ganz einfacher (ASCII-)Text ist gut. MS-Word bringt uns keine Vorteile, wir
würden eh nur den Text extrahieren.

Wenn du es etwas anspruchsvoller gestalten willst, solltest du einen Blick in die
[Beschreibung der nächsten Stufe](../stufe2/)
werfen.
