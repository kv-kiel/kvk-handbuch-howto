---
title: Markdownie
weight: 20
chapter: true
pre: "<b>2. </b>"
date : 2019-12-31T19:01:30+01:00
LastModifierDisplayName :  "BalticPaddler" 
LastModifierEmail : "howto@kv-kiel.de"
---

### Stufe 2

# Markdownie

Du möchtest Inhalt liefern und ihn auch schon in Form bringen? Dann schick uns eine Datei im **Markdown**-Format, denn der Inhalt dieser Web-Seite ist aus einzelnen Dateien im **Markdown**-Format aufgebaut.
Wie du es bewerkstelligen kannst, wohlgeformte **Markdown**-Dateien zu erzeugen, ist auf den folgenden Seiten beschrieben:

{{% children depth="1" style="li" showhidden="true" %}}
