---
title 	: "Markdown-Editoren"
weight  : 20
date                    : 2019-11-15T12:39:29+01:00
LastModifierDisplayName :  "BalticPaddler" 
LastModifierEmail       : "howto@kv-kiel.de"
---


Man kann **Markdown** Dateien mit jedem beliebiegen Editor erstellen oder bearbeiten.
Aber es gibt eine Reihe von Editoren, die einem Unterstützung bei der Bearbeitung bieten.

Ich benutze und empfehle 
[Visual Studio Code](https://code.visualstudio.com)
mit einem entsprechenden Plugin wie z.B: _Markdown Preview Enhanced_:

![VSCode](VSCode.png)

Es gibt aber noch eine Vielzahl anderer Werkzeuge, die man nach eigenem Belieben ausprobieren und nutzen kann, z.B.:

* [Markdown Monster](https://markdownmonster.west-wind.com)
* [Notable](https://notable.md)
* [Zettlr](https://www.zettlr.com)
