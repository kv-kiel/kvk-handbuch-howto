---
title 	: "Markdown-Syntax"
weight  : 10
tags	: []
date  	: 2020-01-08T08:46:47+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---

## Für ganz Ungeduldige

Eine Datei (`beispielseite.md`) mit dem folgenden Inhalt:

```
---
title : "Beispielseite"
---
# Dicke Überschrift
### Absatzüberschrift
Absatztext mit einem **fetten** und einem _kursiven_ Wort --- und einem [Link](https://kv-kiel.de)
```

... ergibt folgende Darstellung:
***
# Dicke Überschrift

### Absatzüberschrift
Absatztext mit einem **fetten** und einem _kursiven_ Wort --- und einem
[Link](https://kv-kiel.de)
***

## Für die etwas Geduldigeren

**Markdown** <i class="fas fa-highlighter"></i>
ist die Bezeichnung für eine sogenannte Markup-Sprache.
Das bedeutet, dass man Formatierungsanweisungen in den Text einbetten kann,
die das Erscheinungsbild bestimmen, wie z.B. Überschriften, Fettdruck, Links, etc.
Von solchen Sprachen gibt es eine ganze Reihe, mein Favorit ist
[AsciiDoc](https://powerman.name/doc/asciidoc),
aber Markdown ist die einfachste und wird von
[Hugo](https://gohugo.io)
am besten unterstützt.

**Hugo** ist das Programm, das aus den gesammelten Texten diese Webseite erzeugt.
Dazu muss allerhand in die Reihe gebracht und arrangiert werden.
Das Erscheinungsbild der Seite nennt man _Theme_. Darin ist die gesamte Struktur,
die Farbgebung und die innere Funktionalität der Seite definiert.

Grundlage sowohl für diese Seite als natürlich auch für das
[KVK-Handbuch](https://kv-kiel.gitlab.io/kvk-handbuch/)
ist das
[Hugo-LearnTheme](https://themes.gohugo.io//theme/hugo-theme-learn/).

Der vorstehende Link ist eine gute Quelle für nähere Informationen über die
Möglichkeiten, die man hat, diese Seite zu gestalten. 
Dort wird eben einiges über die **Markdown**-Syntax gesagt und es sind einige Erweiterungen dafür beschrieben, die zur Verfügung stehen, wenn man dieses _Theme_ nutzt.

Eine weitere gute Einführung in die Möglichkeiten von Markdown (ohne Erweiterungen)
ist auch das Kapitel
[Markdown Syntax von _Grav_](https://learn.getgrav.org/16/content/markdown)
oder die Seite [Basic Markdown Syntax](https://www.markdownguide.org/basic-syntax/).

## Seitenstruktur

Jedes "Informationseinheit" dieser Seite ist in einer eigenen Datei zu Hause.
Das ist eine normale Text-Datei mit der Endung ".md" (was natürlich für **Markdown** steht).

Grob gesagt, besteht der Inhalt so einer Datei aus zwei Teilen: dem _FrontMatter_ und dem eigentlichen Informationsinhalt.
Die Datei sollte genauso heißen, wie der Titel - nur alles in Kleinbuchstaben --- und hinten `.md`.

### _FrontMatter_

Im _FrontMatter_ stehen _Meta-Informationen_ über die Datei. Die _FrontMatter_ für die Datei, die den Text beinhaltet, den du gerade liest, sieht folgendermaßen aus:

```
---
title                   : "Markdown-Syntax"
tags                    : []
ressorts                : []
date                    : 2019-11-15T12:39:29+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---
```

Der einzig wichtige Eintrag ist der, der mit `title:` beginnt. Wenig überraschend ist das, was in den Tüddeln danach folgt, der Titel der Seite ;-).

Die anderen Einträge sollten aber nach Möglichkeit auch enthalten sein, denn sie haben alle ihre spezische Bedeutung und Aufgabe.
Die beiden Einträge `tags` und `ressorts` dienen der Taxonomie, die weiter unten detaillierter beschrieben ist.

Unten auf jeder Seite steht ein kleiner Eintrag, wer die entsprechende Datei wann das letzte Mal geändert hat:

![LastModified](LastModified.png)

Dieser Eintrag wird aus den drei Feldern `date`, `LastModifierDisplayName` und `LastModifierEmail` gebildet.
Der Eintrag `date` sollte daher immer das Datum der letzten Änderung beinhalten.
In `LastModifierDisplayName` kann man angeben, wer als Autor der letzten Änderung genannt werden soll.
Hier sollte der eigene Name oder ein Pseudonym stehen.
In `LastModifierEmail` steht die EMail-Adresse, die für den Link zur EMail benutzt wird.
Hier sollte einfach der Wert `howto@kv-kiel.de` stehen.

### Informationsinhalt

Nach der _FrontMatter_ kommt beliebiger Text. Damit der nicht so furzlangweilig daherkommt, kann man ihn eben mit **Markdown**-Formatanweisungen aufpeppen.

Hier ein kleines Beispiel. Der folgende "rohe" Text:
```
### Absatzüberschrift
Absatztext mit einem **fetten** und einem _kursiven_ Wort --- und einem [Link](https://kv-kiel.de)
und einer Tabelle:

| Spalte1 | Spalte2 | Spalte3 |
|---------|---------|---------|
| Wert 1  | Wert 2  | Wert 3 |

... und einem Bild
![Womanizer](womanizer-400x268.jpg)

```

liefert das folgende Ergebnis:
***
### Absatzüberschrift
Absatztext mit einem **fetten** und einem _kursiven_ Wort --- und einem [Link](https://kv-kiel.de)
und einer Tabelle:

| Spalte1 | Spalte2 | Spalte3 |
|---------|---------|---------|
| Wert 1  | Wert 2  | Wert 3 |

... und einem Bild
![Womanizer](womanizer-400x268.jpg)

***

## Taxonomie

Die Beiträge zum **KVK-Handbuch** sind in zwei Dimensionen "klassifizierbar".
Sie können mit Stichwörtern (_Tags_) versehen werden, und sie können sogenannten "Ressorts" zugeordnet werden.

Im **KVK-Handbuch** hat die Seite mit dem Titel "Arbeitsdienst" ein Stichwort "Arbeitsdienst" und ist den Ressorts "Bauausschuss" und "Mitgliederwartin" zugeordnet:

![Taxonomie](Taxonomie.png)

Diese "Kategorisierung" wird über entsprechende Einträge im _FrontMatter_ der Datei bewerkstelligt. Hier der entsprechende Teil der zugehörigen Datei:

```
---
title   : "Arbeitsdienst"
weight  : 20
tags    : [ Arbeitsdienst ]
ressorts: [ Bauausschuss, Mitgliederwartin ]
date    : 2020-01-07T20:51:00+01:00
LastModifierDisplayName :  "BalticPaddler"
LastModifierEmail : "howto@kv-kiel.de"
---

```
{{% notice warning %}}
Es ist wichtig, die Attribute "tags" und "ressorts" im _FrontMatter_ mit kleinen Buchstaben zu schreiben!
{{% /notice %}}

### Stichwörter

Die Werte für die Stichwörter (_Tags_) sind nicht vorgegeben, es können beliebige Begriffe verwendet werden. Lieber mehr angeben als zu wenig.

### Ressorts

Es sollten nach Möglichkeit nur die Ressorts angegeben werden, die es in unserem Verein auch gibt! Das sind

* "1. Vorsitzende"
* "2. Vorsitzende"
* Kassenwartin
* Ältestenrat
* Bauausschuss
* Bootshallenwartin
* Festschuss
* Jugendwartin
* Mitgliederwartin
* Pressewartin
* Schriftwartin
* Sportwartin
* Wanderwartin

Diese Ressorts müssen genau so angegeben werden. Leider ist **Hugo** nicht so schlau zu erkennen, dass es sich bei "Jugendwartin" und "Jugendwart" eigentlich um dasselbe Ressort handelt!

{{% notice tip %}}
Die Ressort-Angaben für 1. bzw. 2. Vorsitzende müssen nicht wie oben in Tüddel geklammert werden, können aber so angegeben werden. Wenn ich sie hier ohne Tüddel geschrieben hätte, sähe das Layout nicht so gut aus, weil **Markdown** "denkt", dass es sich dabei um eine Aufzählung handelt :-/
{{% /notice %}}

### Spezialitäten

Eine speziell für dieses Theme vorhandene Erweiterung von Markdown ist die Möglichkeit, bunte Blöcke zu erzeugen. Die gibt es in den drei Geschmacksrichtungen "tip", "info" und "warning". Sie werden erzeugt durch Eingabe eines Blockes der folgenden Art:

![](notice-block.png)

Wenn man den Selektor "info" in ersten Zeile durch eine der anderen Geschmacksrichtungen ersetzt, erhält man den entsprechend eingefärbten Block:

{{% notice tip %}}
TIPP
{{% /notice %}}
{{% notice info %}}
INFO
{{% /notice %}}
{{% notice warning %}}
WARNUNG
{{% /notice %}}
