---
title 	: "Vom Text zur Webseite"
weight  : 10
date  	: 2019-11-15T12:39:29+01:00
LastModifierDisplayName :  "BalticPaddler" 
LastModifierEmail : "howto@kv-kiel.de"
---

Aus dem Studium der vorhergehenden Stufen weißt du, dass der Inhalt des
[KVK-Handbuchs](https://kv-kiel.gitlab.io/kvk-handbuch/)
aus einzelnen **Markdown**-Dateien aufgebaut ist.

Da nicht nur eine einzelne Person mit ihrem privaten Rechner in der Lage sein soll, die Inhalte zu ändern oder zu ergänzen, müssen die **Markdown**-Dateien auf einem für jeden Bearbeiter zugänglichen Server liegen.

**Markdown**-Dateien alleine sind aber noch keine Web-Seite. Damit eine daraus wird, müssen die Dateien entsprechend verarbeitet werden (wie bereits im Kapitel [Markdown-Syntax](/Stufe2/markdown-syntax) erwähnt, übernimmt das Programm **Hugo** diese Arbeit).

Und damit die Web-Seite für jeden aufrufbar ist, müssen die verarbeiteten Dateien auf einem öffentlich zugänglichen Server liegen.

Es müssen also drei Bedingungen erfüllt sein:

1. Die Inhaltsdateien müssen für alle autorisierten Personen zugänglich sein.
2. Die Inhaltsdateien müssen zu einer Web-Seite aufbereitet werden.
3. Die aufbereiteten Dateien müssen auf einem allgemein zugänglichen Server liegen.

Um alle diese drei Bedingungen zu erfüllen, nutzen wir **GitLab**.

[GitLab](https://gitlab.com) ist eine Webanwendung zur Versionsverwaltung für Softwareprojekte auf Basis von Git.
Etwas weniger technisch formuliert: Alle Texte, die für diese Seite (oder die Seite für das [KVK-Handbuch](https://kv-kiel.gitlab.io/kvk-handbuch/) ) verwendet werden, sind bei **GitLab** hinterlegt ("gehostet" wie man neu-deutsch sagt) (**Bedingung 1**).

Jedesmal, wenn eine Datei bei GitLab geändert wird, wird ein Verarbeitungsprozess angestoßen, der **Hugo** auffordert, die vorliegenden Dateien in die entsprechende Form einer Web-Seite zu bringen (**Bedingung 2**).

Am Schluss der Umformung wird die Web-Seite an einen Ort kopiert, der öffentlich zugänglich ist (**Bedingung 3**).

Neben den drei oben genannten Bedingungen bietet **GitLab** zusätzlich noch eine ganze Reihe von weiteren Möglichkeiten, die aber vorerst nicht relevant sind.
