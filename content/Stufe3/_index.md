---
title   : "GitLab-Junkie"
weight  : 30
pre     : "<b>3. </b>"
chapter : true
date : 2019-12-31T19:01:30+01:00
LastModifierDisplayName :  "BalticPaddler" 
LastModifierEmail : "howto@kv-kiel.de"
---

### Stufe 3

# GitLab-Junkie

Du möchtest nicht nur liefern, sondern deine Lieferung auch selbst ins System bringen?
Kein Problem! Du brauchst nicht einmal spezielle Software auf deinem Rechner zu installieren, du kannst alles über einen Browser erledigen!

Folgende Artikel werden dir weiterhelfen:

{{% children depth="1" style="li" showhidden="true" %}}
