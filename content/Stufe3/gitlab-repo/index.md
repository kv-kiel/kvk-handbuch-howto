---
title 	: "GitLab Repository"
weight  : 20
date : 2019-12-31T19:01:30+01:00
LastModifierDisplayName :  "BalticPaddler" 
LastModifierEmail : "howto@kv-kiel.de"
---

## Browsen

Alle zum KVK-Handbuch gehörenden Dateien sind in einem sogenannten
[Repository bei GitLab](https://gitlab.com/kv-kiel/kvk-handbuch)
 gehosted.

{{% notice note %}}
[Das Repository, das zu dieser "Howto"-Seite gehört](https://gitlab.com/kv-kiel/kvk-handbuch-howto),
ist natürlich ein anderes- aber alle Aussagen zum Handbuch gelten natürlich genauso für dieses Howto!
{{% /notice %}}

In dem Repository kannst du dir mit einem Web-Browser alle Dateien anzeigen lassen und in sie hineinsehen.

## Schreiben

Nachdem du die Bestätigungsmail vom Administrator erhalten hast, dass du als Teammitglied aufgenommen bist,
kannst du die Dateien nicht nur ansehen, sondern sie auch ändern.

Eine besonders einfache und elegante Möglichkeit, eine bestimmte Datei zu ändern, ist,
den auf jeder Seite oben rechts angezeigten Link **Bearbeite diese Seite** zu klicken.
Damit wirst du direkt zu der GitLab-Seite geleitet, die dir ein Editieren der
aktuellen Datei erlaubt
(eventuell musst du dich erst noch mit deinem GitLab-Account anmelden).

![gitlab-edit](gitlab-edit.png)

## Committen

Wenn du deine Änderungen gemacht hast, muss du den "Commit changes"-Knopf unten drücken. Am besten gibst du im Feld "Commit Nachricht" auch noch eine kurze Beschreibung ein, was du geändert hast.

Damit sind deine Änderungen gespeichert und können im System verwendet werden

## Merge-Request erstellen

(siehe auch [Merge-Request](http://localhost:1313/stufe4/git/#mergerequest))

Es ist noch eine Hürde zu nehmen, bevor deine Änderungen auch wirklich wirksam werden.
dann entscheidet, ob sie übernommen wird oder nicht.
Bei unserem Handbuch wäre eine fehlerhafte Änderung nicht weiter tragisch ---
allerhöchstens funktioniert die Web-Seite nicht mehr korrekt.
Das kann bei anderen Projekten durchaus schlimme Folgen haben und daher wird generell so verfahren.

Den Merge-Request musst du jemandem zuweisen, damit der benachrichtigt wird, den Request zu bearbeiten.
Wenn der Reuest dann akzeptiert und "gemerget" worden ist, wird automatisch die Verarbeitung
der **Markdown**-Dateien zur Web-Seite angestoßen und du kannst dir das Ergebnis über die
[öffentliche URL des Handbuches](https://kv-kiel.gitlab.io/kvk-handbuch//)
ansehen.
