---
title   : "GitLab Account"
weight  : 10
LastModifierDisplayName :  "BalticPaddler" 
LastModifierEmail       : "howto@kv-kiel.de"
---

Um bei **GitLab** Dateien editieren zu können, benötigt man ein Konto, für das man sich [bei GitLab registrieren](https://gitlab.com/users/sign_up) muss:
![GitLabRegister](gitlab-register.png)

Um dann am [KVK-Handbuch](https://kv-kiel.gitlab.io/kvk-handbuch/) mitarbeiten zu dürfen, muss der Administrator dein eben erstelltes Konto zum Projekt hinzufügen (sonst könnte ja jeder ungefragt an unserem Projekt rummachen!).

Wenn der Administrator dir den Zugriff eingerichtet hat, bekommst du eine Mail, die etwa folgendermaßen aussieht:
![AccessGranted](AccessGranted.png)

Du bist damit als Teammitglied für das Projekt aufgenommen und in der Lage, beliebige Dateien im Handbuch zu ändern, neue hinzuzufügen oder solche, die dich stören, zu löschen.

{{% notice note %}}
Keine Angst! Du kannst nichts kaputt machen! Durch die eingebaute Versionsverwaltung ist man jederzeit in der Lage, einen beliebigen Stand des Handbuches wiederherzustellen!
{{% /notice %}}
