---
title: Nerdie
chapter: true
pre: "<b>4. </b>"
weight: 40
date  	: 2019-11-15T12:39:29+01:00
LastModifierDisplayName :  "BalticPaddler" 
LastModifierEmail : "howto@kv-kiel.de"
---

### Stufe 4

# Nerdie

Du möchtest den Laden übernehmen? Prima!

Du solltest dich entweder einigermaßen mit Software-Entwicklung auskennen oder einfach keine Scheu vor Computern uns Software haben. Die wesentlichen Werkzeuge, die du benötigst, sind im folgenden beschrieben:

{{% children depth="1" style="li" showhidden="true" %}}
