---
title                   : "Git"
tags                    : []
date                    : 2020-01-04T18:29:19+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
weight                  : 10
---

## Überblick

Die Original-Quellen für das
[KVK-Handbuch](https://kv-kiel.gitlab.io/kvk-handbuch/)
sind im Internet in einem
[Git-Repository](https://www.git-scm.com/book/de/v2)
gespeichert. Dieses Repository wird von
[GitLab](https://gitlab.com)
gehostet:
[https://gitlab.com/kv-kiel/kvk-handbuch-howto](https://gitlab.com/kv-kiel/kvk-handbuch-howto)

Wenn man am Projekt mitarbeitet, verfügt man über eine lokale Kopie des Repositories auf dem eigenen Rechner.

Der grundsätzliche Ablauf zum Ändern von Dateien sieht ungefähr wie folgt aus:

1. Ihr gleicht euer lokales Repository mit dem des Servers ab.
1. Ihr nehmt Änderungen an der lokalen Kopie des Repositories auf eurem eigenen Rechner vor.
1. Ihr *committed* und *pusht* sie zum Server.
1. Ihr erstellt einen *Merge-Request* und weist ihn einem anderen Mitentwickler zu.
1. Der andere Mitentwickler überprüft eure Änderungen auf Unbedenklichkeit und akzeptiert sie.
1. Eure Änderungen werden in die Online-Version des KVK-Handbuch eingearbeitet.

## Installation von Git

Natürlich muss man *Git* installieren, bevor man es benutzen möchte. Dazu müsst ihr es zuerst bei
[Git runterladen](https://git-scm.com/downloads)
und dann das Setup ausführen. Während des Installationsprozesses kann man seinen bevorzugten Editor auswählen. Hier empfehle ich
[Visual Studio Code](https://code.visualstudio.com/download).
(Den Standard-Editor *vim* möchten vielleicht nur die ganz unerschrockenen benutzen!).

Bei der Frage nach der Pfad-Anpassung solltet ihr die zweite (empfohlene) Option wählen:
![InstallGit1](InstallGit1.png)

Die restlichen Optionen könnt ihr so übernehmen wie vorgeschlagen.

## Zugang zu Gitlab

Um das Repository nicht nur lesen sondern wirklich mit ihm arbeiten zu können, müsst ihr ein
[Konto bei GitLab eröffnen](https://gitlab.com/users/sign_in):

![GitLabSignIn](GitLabSignIn.png)

### SSH-Schlüssel

Um schreibende Rechte bei GitLab zu haben, muss man sich als berechtigter Benutzer identifizieren.
Man könnte sich jedes Mal über die Web-Oberfläche einloggen, was für einzelne, klitzekleine Änderungen mal akzeptabel sein kann, aber für flüssiges Arbeiten ist das keine praktikable Option. Daher nutzt man *SSH* (Secure Shell) für diese Aufgabe.

Unter Windows 10 ist SSH bereits installiert, das macht die Sache einfach. Mit dem Kommando:

```
ssh-keygen -t rsa -b 4096
```

wird ein SSH-Schlüssel erzeugt (die Aufforderung, eine Passphrase einzugeben, wird einfach mit *Enter* quittiert):

![ssh-keygen](ssh-keygen.png)

Auf die Frage, wo das entstehende Paar von Dateien gespeichert werden, gibt man einen Dateinamen im Verzeichnis **%USERPROFILE%\\.ssh** an (hier C:\\Users\\Mathias\\.ssh\\id_rsa_gitlab)).

Im Verzeichnis .ssh sind nun zwei Dateien entstanden
![ssh-keygen](list.ssh.png).

Die Datei *id_rsa_gitlab* ist der private Teil des Pärchens und verlässt deinen Rechner nie.
Die andere Datei *id_rsa_gitlab.pub* ist der öffentliche Teil, deren Inhalt du auf dem GitLab-Server in deinen Einstellungen hinterlegen musst.

### Anpassungen an SSH-Konfiguration

Um etwaigen Problemen mit Kollisionen aus dem Weg zu gehen, die entstehen, wenn du SSH nicht nur für diesen Zweck benutzt, wird die Konfiguration von SSH gleich so modifiziert, dass auch anderweitige Nutzungen möglich sind.

In der Datei

```
%USERPROFILE%\.ssh\config
```

wird der folgende Block eingetragen:

```
Host gitlab-kvk
  HostName gitlab.com
  User git
  IdentityFile C:/Users/Mathias/.ssh/id_rsa_gitlab
  IdentitiesOnly yes
```

{{% notice warning %}}
Statt *C:/Users/Mathias/* muss natürlich der Pfad zu eurem Home-Verzeichnis eingetragen werden!
{{% /notice %}}

## Klonen des Repositories

Mit den bisher getätigten Maßnahmen solltest du das Repository von GitLab klonen können.
Dazu öffnest du eine DOS-Shell (oder Power-Shell oder Git-Bash) und wechselst in ein Verzeichnis, unterhalb dessen du die Quellen für das KVK-Handbuch haben möchtest. Dann setzt das folgende Kommando ab:

```
git clone git@gitlab-kvk:kv-kiel/kvk-handbuch.git
```

Nach einigen Log-Ausgaben sollte schließlich das Unterverzeichnis *kvk-handbuch* entstanden sein.

## Aufnahme in den Kreis der Autoren

Alle bisher beschriebenen Schritte kann jeder nach Belieben durchführen - egal, ob wir ihn kennen oder sie uns wohlgesonnen ist. Damit nicht jeder ohne Ansehen am Inhalt unseres Handbuch Änderungen vornehmen kann, muss es noch einen Vorgang der "Segnung" eines neuen Autors geben.

Dazu musst du dem Maintainer des Handbuchs eine
[Mail](mailto:howto@kv-kiel.de) schicken, in der du ihn um Aufnahme als Entwickler bittest. Wenn das geschehen ist, kannst du fröhlich neue Beiträge liefern oder bestehende modifizieren.

## Workflow

Hier wird der Ablauf, der ganz oben kurz skizziert wurde, etwas detaillierter beschrieben.

### Pull

Vor dem Beginn jeder Arbeit an den Quellen solltest du sicherstellen, dass die Quellen auf deinem Rechner auf dem gleichen Stand sind wie die auf dem GitLab-Server. Dazu musst du im Verzeichnis *kvk-handbuch* ein

```
git pull --rebase
```

durchführen. Nun könnt ihr an den Quellen arbeiten.

### Modifikationen vornehmen

Ihr könnt einen Editor eurer Wahl nehmen, ich empfehle
[Visual Studio Code (VSCode)](https://code.visualstudio.com/download).
Da ist Git gleich mit integriert - und auch sonst alles, was man sich so wünschen kann.

Wenn ihr fertig seid mit euren Änderungen, müssen diese *committed* werden. Das geht mit VSCode ganz einfach:

![VSCode-Git-Commit](VSCode-Git-Commit.png)

Nun muss man realisieren, dass ein *Commit* erst einmal nur lokal auf deinem Rechner stattfindet und sozusagen einen gesicherten Stand erzeugt. Man könnte nun durchaus weiter editieren und später einen erneuten *Commit* durchführen.

Damit die Änderungen auf den Server gelangen, muss ein *git push* durchgeführt werden.

### Modifikationen zum Server beamen

Theroretisch kann während du Modifikationen durchgeführt hast, auch jemand anders etwas in das Repository eingespielt haben (die Wahrscheinlichkeit ist nicht sehr hoch, solange nicht Dutzende Autoren am Handbuch mitarbeiten ;-) ). Daher solltest du nochmals ein *git pull --rebase* wie oben durchführen. Danach bringst du mit einem

```
git push
```

deine Modifikationen auf den Server.

### Merge-Request

(siehe auch
[MergeRequest-erstellen](../../stufe3/gitlab-repo/#mergerequest-erstellen) )

Mit deinem *Push* läuft auf dem Server automatisch eine syntaktische Überprüfung der Quellen an. Allerdings wird der jetzt aktuelle Stand noch nicht in die endgültige Online-Version des Handbuchs übernommen. Hier ist noch einmal eine Sicherheitsstufe eingebaut, dass nämlich ein zweiter Entwickler einen Blick auf deine Änderungen werfen muss. Dazu musst du einen *Merge-Request* erstellen. Das geschieht über die [Web-Oberfläche von GitLab](https://gitlab.com/kv-kiel/kvk-handbuch/merge_requests):

![GitLabMergeRequest](GitLabMergeRequest.png)

Nach Drücken auf den *New Merge Request*-Knopf müsst ihr im folgenden Dialog für den Source-Branch *development* auswählen und für den Target-Branch *master*:

![GitLabNewMergeRequest](GitLabNewMergeRequest.png)

Bei den Details zum Merge-Request solltet ihr einen *Assignee* auswählen
![GitLabNewMergeRequest2](GitLabNewMergeRequest2.png)
und dann unten auf der Seite auf *Submit merge request* drücken.

Der Assignee bekommt dann eine Mail und wird sich der Sache annehmen. Wenn der Merge-Request akzeptiert worden ist, wird automatisch der Prozess gestartet, der die neuen Quellen in die Online-Version des Handbuches übernimmt.
