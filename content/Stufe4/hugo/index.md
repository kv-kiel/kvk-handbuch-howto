---
title                   : "Hugo"
tags                    : []
date                    : 2020-01-05T01:34:21+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
weight                  : 20
---

<!-- TOC -->

- [1. Überblick](#1-%c3%9cberblick)
- [2. Installation](#2-installation)
- [3. HUGO anwenden](#3-hugo-anwenden)
  - [3.1. Cross-Referencing](#31-cross-referencing)
  - [3.2. Einbinden von Bildern](#32-einbinden-von-bildern)

<!-- /TOC -->

<a id="markdown-1-überblick" name="1-überblick"></a>
## 1. Überblick

Das Tool
[HUGO](https://gohugo.io)
dient dazu, aus den Quellen (also im wesentlichen den Markdown-Dateien) und den zugehörigen Konfigurations- und Layout-Datein eine gefällige und funktionale Webseite zu erstellen.
Das geschieht jedesmal, wenn auf dem GitLab-Server Dateien geändert werden (für den genauen Ablauf siehe im Kapitel [Git](../git/index.md) ).

Um zu sehen, wie die eigenen Änderungen in der fertigen Webseite aussehen, kann man *HUGO* auch auf dem lokalen Server einsetzen. Dazu muss das Tool installiert und dann benutzt werden.

Eine gute Referenz ist die [Dokumentation von HUGO](https://gohugo.io/documentation/) bzw. für den Anfang das [Getting Started](https://gohugo.io/categories/getting-started).

{{% notice tip %}}
Man kann ganz ohne *HUGO* auf dem lokalen Rechner auskommen, aber es ist sehr hilfreich sofort beurteilen zu können, wie die erzeugten oder modifzierten .md-Dateien aussehen.
{{% /notice %}}

<a id="markdown-2-installation" name="2-installation"></a>
## 2. Installation

[Download](https://github.com/gohugoio/hugo/releases)

Einfach die Datei *hugo_\*_Windows-64bit.zip* runterladen und ein ein Verzeichnis deiner Wahl entpacken - z.B. *C:\\Tools\Hugo*.

<a id="markdown-3-hugo-anwenden" name="3-hugo-anwenden"></a>
## 3. HUGO anwenden

```
C:\tools\Hugo\hugo.exe server --baseURL http://localhost/kvk-handbuch
```

Neue Seite mit Hugo anlegen:
```
C:\tools\Hugo\hugo.exe new 01-verein\booshallenschluessel.md
```

{{% notice warning %}}

Es ist sorgfältig darauf zu achten, dass nur Kleinbuchstaben verwendet werden!
Das habe ich nicht von aAnfang an konsequent gemacht und mir dadurch einige ärgerliche Probleme eingehandelt!
{{% /notice %}}

<a id="markdown-31-cross-referencing" name="31-cross-referencing"></a>
### 3.1. Cross-Referencing

Wenn man in einer Markdown-Datei Referenzen (Links) auf andere lokale Markdown-Dateien einbauen möchte, so geht prinzipiell wir bei einem normalen Link mit `[Link-Name](Link-Ziel)`. Da aber die Struktur der Dateien von `Hugo` massive umgestaltet wird, ist es am verlässlichsten, die `relref`-Funktion von Hugo zu benutzen. Einen Link auf die Seite der `2. Vorsitzenden` würde bspw. durch das folgende Konstrukt verwirklicht werden:

![](cross-ref.png)

<a id="markdown-32-einbinden-von-bildern" name="32-einbinden-von-bildern"></a>
### 3.2. Einbinden von Bildern

Die Seiten, die den Inhalt einer Web-Seite in Hugo enthalten, sind im Dateibaum unterhalb des Verzeichnisses `content` angeordnet. Hier gibt es  zwei grundsätzlich unterschiedliche Arten von Seiten in Hugo:

- Solche, die durch eine einzelne Datei dargestellt werden
- und solche, die durch ein eigenes Unterverzeichnis dargestellt werden.

Ein Beispiel für eine Seite, die durch ein Verzeichnis dargestellt wird, ist die Seite [Verein](https://kv-kiel.gitlab.io/kvk-handbuch/01-verein/). Natürlich hat diese Seite neben ihrem textuellen Inhalt auch eine "innere Struktur" - enthält also weitere Seiten, daher ist es plausibel, die Repräsentation dieser Seite über ein Verzeichnis zu realisieren. Der Text dieser Seite ist in der Datei `_index.md` enthalten.

Der Text aller enthaltenen Unterseiten wird durch die in diesem Verzeichnis liegenden Elemente repräsentiert.
Haben diese Unterseiten keine weitere innere Struktur, werden sie durch einfache Dateien (mit der Endung `.md`) wiedergegeben, haben sie ebenfalls eine Struktur, dann durch ein ein Verzeichnis.

Leider ist nun die Angabe für den Ort eines Bildes unterschiedlich, je nachdem, ob man es in einer Datei `_index.md` angibt, oder in einer x-beliebigen anderen Datei.

Liegt das zu referenzierende Bild im selben Verzeichnis, wie die Datei, dann ist in der Datei `_index.md` der Name des Bildes direkt anzugeben, in allen anderen Dateien aber muss ein `../` davor gesetzt werden.

Pfadangaben zu einem Bild in `_index.md`:
```
![Bild1](NameDerBildDatei1.jpg)
```

Pfadangaben zu einem Bild in einer beliebigen Datei:
```
![Bild2](../NameDerBildDatei2.jpg)
```