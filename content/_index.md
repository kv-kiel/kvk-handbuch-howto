---
title: "KVK-Handbuch-Howto"
date : 2019-12-31T19:01:30+01:00
LastModifierDisplayName :  "BalticPaddler" 
LastModifierEmail : "howto@kv-kiel.de"
---

# KVK-Handbuch-Howto

## Du hast Interesse am KVK-Handbuch mitzuarbeiten? 

Diese Seite stellt alle Informationen zusammen, die du benötigst, um zum
[KVK-Handbuch](https://kv-kiel.gitlab.io/kvk-handbuch/)
beitragen zu können!

Je nach Vorwissen und Interesse, kannst du deine Mitarbeit in  unterschiedlichen
Stufen gestalten. Je weiter du dich auf der Leiter hinauf wagst, desto mehr Arbeit
nimmt du uns ab. Aber es ist vollkommen okay, wenn du erst einmal mit einfachen 
Mitteln anfangen möchtest.

Die Stufen sind folgende:
{{% children depth="1" style="li" showhidden="false" %}}
