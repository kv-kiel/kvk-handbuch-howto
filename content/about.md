---
title    : "Über uns"
subtitle : "... und warum du nichts mit uns zu tun haben möchtest ..."
hidden   : true
date : 2019-12-31T19:01:30+01:00
LastModifierDisplayName :  "BalticPaddler" 
LastModifierEmail : "howto@kv-kiel.de"
---

{{% notice note %}}
Nennt mich Ismael!
{{% /notice %}}

{{% notice tip %}}
Hier eine umfassende Beschreibung:
{{% /notice %}}

{{% notice info %}}
Information
- Ich habe einen Bart
- Ich bin extrem langsam
- Ich bin 42!
{{% /notice %}}

