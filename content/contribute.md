---
title   : "Zu dieser Webseite beitragen"
hidden  : true
date : 2019-12-31T19:01:30+01:00
LastModifierDisplayName :  "BalticPaddler" 
LastModifierEmail : "howto@kv-kiel.de"
---

Diese Webseite ist mit
[Hugo](https://gohugo.io/)
erstellt und wird auf
[Gitlab](https://gitlab.com/kv-kiel/kvk-handbuch-howto/)
gehostet.

Das Theme, auf dem die Seite basiert, ist
[hugo-theme-learn](https://themes.gohugo.io/theme/hugo-theme-learn/).

Da die Seite genauso aufgebaut ist wie das
[KVK-Handbuch](https://kv-kiel.gitlab.io/kvk-handbuch/)
und beschreibt, wie man das Handbuch modifiziert, beschreibt sie also auch, wie man sie selbst modifiziert ;-)
